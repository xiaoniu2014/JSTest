//
//  ViewController.m
//  JSTest
//
//  Created by 洪伟 on 16/6/12.
//  Copyright © 2016年 hongw. All rights reserved.
//

#import "ViewController.h"
#import <JavaScriptCore/JavaScriptCore.h>

/**参考资料
 *  http://www.jianshu.com/p/f896d73c670a#
 */

@protocol JSObjcDelegate <JSExport>

- (void)callCamera;
- (void)share:(NSString *)shareString;

@end

@interface ViewController ()<UIWebViewDelegate,JSObjcDelegate>
@property (weak, nonatomic) IBOutlet UIWebView *webView;
@property (strong, nonatomic) JSContext *jsContext;

@end

@implementation ViewController

#pragma mark - Lift Cycle

- (void)viewDidLoad {
    [super viewDidLoad];
//    NSString *html = @"href";
    NSString *html = @"test";
    NSURL *url = [[NSBundle mainBundle] URLForResource:html withExtension:@"html"];
    [self.webView loadRequest:[NSURLRequest requestWithURL:url]];
    
}

#pragma mark - Delegate
- (BOOL)webView:(UIWebView *)webView shouldStartLoadWithRequest:(NSURLRequest *)request navigationType:(UIWebViewNavigationType)navigationType{
    NSString *url = request.URL.absoluteString;
    if ([url hasPrefix:@"toyun://"]) {
        // url的协议头是toyun
        NSLog(@"callCamera");
        return NO;
    }
    return YES;
}
- (void)webViewDidFinishLoad:(UIWebView *)webView{
    self.jsContext = [webView valueForKeyPath:@"documentView.webView.mainFrame.javaScriptContext"];
    self.jsContext[@"Toyun"] = self;
    self.jsContext.exceptionHandler = ^ (JSContext *context, JSValue *exception){
        context.exception = exception;
        NSLog(@"异常信息：%@",exception);
    };
}

- (void)webView:(UIWebView *)webView didCreateJavaScriptContext:(JSContext*) ctx{
    
}

#pragma mark - JSObjcDelegate

- (void)callCamera {
    NSLog(@"callCamera");
    // 获取到照片之后在回调js的方法picCallback把图片传出去
    JSValue *picCallback = self.jsContext[@"picCallback"];
    [picCallback callWithArguments:@[@"photosSSS"]];
}

- (void)share:(NSString *)shareString {
    NSLog(@"share:%@", shareString);
    // 分享成功回调js的方法shareCallback
    JSValue *shareCallback = self.jsContext[@"shareCallback"];
    [shareCallback callWithArguments:nil];
}

@end
