//
//  BaseViewController.m
//  JSTest
//
//  Created by 洪伟 on 16/6/12.
//  Copyright © 2016年 hongw. All rights reserved.
//

#import "BaseViewController.h"

@interface BaseViewController ()

@end

@implementation BaseViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
//    self.edgesForExtendedLayout = UIRectEdgeAll;
    self.automaticallyAdjustsScrollViewInsets = NO;
}

@end
